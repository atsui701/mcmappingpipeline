#!/bin/bash

function usage
{
    echo "Usage: $0 [subjects|protocols]"
    exit 1
}

if [[ $# < 1 ]]; then
    usage
fi

if [[ $1 == "subjects" ]]; then
    for i in `ls subjects`; do
        echo `basename $i`
    done
elif [[ $1 == "protocols" ]]; then
    for i in `ls protocols | grep -v -E "common|template"`; do
        echo `basename $i`
    done
else
    usage
fi
