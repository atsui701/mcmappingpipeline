#!/bin/bash
# Compute a hyperbolic metric.
#
# Parameters:
#   SUBJECT - subject
#   PROTOCOL - protocol
#
# Input:
# ${SUBJECT}/mesh.off (non-pants protocols)
# ${SUBJECT}/protocols/${PROTOCOL}/mesh.off (pants protocols)
# ${SUBJECT}/protocols/${PROTOCOL}/cones.asc (depends on protocol)
#
# Output:
# ${SUBJECT1}/protocols/${PROTOCOL}/metric.u
function usage
{
    echo "Usage: $0 [-nc] subject protocol"
    exit 1
}

DRY_RUN=false
CHECK=false
while getopts ":nc" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    c)
        CHECK=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 2 ]]; then
    usage
fi

SUBJECT=$1
PROTOCOL=$2
source protocols/${PROTOCOL}/paths.txt

if $IS_PANTS_PROTOCOL; then
MESH=subjects/${SUBJECT}/protocols/${PROTOCOL}/mesh.off
else
MESH=subjects/${SUBJECT}/mesh.off
fi
OUT=subjects/${SUBJECT}/protocols/${PROTOCOL}/metric.u

if $CHECK; then
    if [[ ! -e ${OUT} ]]; then
        echo ${SUBJECT} missing metric.u
    fi
    exit 0
fi

if [ -n "$ORBIFOLD_CONES" ]; then
    CONE_OPTION="-c ${ORBIFOLD_CONES} -a ${ORBIFOLD_ANGLE_FACTOR}"
fi

if $IS_PANTS_PROTOCOL; then
if $DRY_RUN; then
echo computeMetricOpen -m ${MESH} -o ${OUT} ${CONE_OPTION}
else # DRY_RUN
computeMetricOpen -m ${MESH} -o ${OUT} ${CONE_OPTION}
fi # DRY_RUN
else # IS_PANTS_PROTOCOL
if $DRY_RUN; then
echo computeMetric -m ${MESH} -o ${OUT} ${CONE_OPTION}
else # DRY_RUN
computeMetric -m ${MESH} -o ${OUT} ${CONE_OPTION}
fi # DRY_RUN
fi # IS_PANTS_PROTOCOL
