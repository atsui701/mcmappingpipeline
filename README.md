General mapping pipeline
=====
This is a set of scripts that sets up the [MeshCutter](https://bitbucket.org/atsui701/meshcutter) command-line scripts in a pipeline for mapping between landmarked meshes. In this project, we refer to mesh instances as **subjects**, and the geometry and landmark data are stored under the `subjects` directory. The general surface mapping pipeline follows this outline:

0. Prepare the mesh and landmark data in a standard directory layout.
1. Compute the hyperbolic metric.
2. Compute an initial map.
3. Perform relaxation using the hyperbolic metric to optimize the initial map.

The details of the relaxation method and constraints are determined by **protocols** stored under the `protocols` directory. Currently, there are two generic approaches: point-based and curve-based. The point-based enforces exact matching of a number of point landmarks, while the curve-based method exactly matches curve landmarks. The *template-o* protocol gives an example of a point-based protocol, whereas *template-p* is an example of a curve-based mapping. Custom protocols can start by copying from these.

The top-level script `stage1.sh` performs the first two steps of the pipeline on a single subject, while `stage2.sh` performs the last two steps between two subjects. As a quickstart example, you can run the following to map *sphere* onto *sphere2* via the *template-o* protocol.

    ./stage1.sh sphere template-o
    ./stage1.sh sphere2 template-o
    ./stage2.sh sphere sphere2 template-o

Directory structure overview
-----

       |-protocols
       | `-${PROTOCOL}
       `-subjects
         `-${SUBJECT}
           |-landmarks
           | |-curves
           | `-points
           `-protocols
             `-${PROTOCOL}
               |-cuts
               |-maps
               | `-${SUBJECT}
               `-seam

Scripts are run from the top level directory, loading inputs and saving outputs
to predetermined locations with respect to this directory structure. See the
script headers for more information.

Protocol setup
-----
Add a folder named with the protocol name in the protocols/ folder in the root
directory. You should have:

- firstCornerOrdering.txt - if you use the n-gon initial map method.
- genscp.sh - specify map type and constraints
- import.sh - specify actions and files to copy prior to stage1.
- landmark_tree.txt - specify additional paths between landmark curves needed to form a landmark tree.
- paths.txt - General variable cache sourced by the scripts for protocol-specific locations.
- scaffold.txt - if you want to create additional subfolders per subject.
