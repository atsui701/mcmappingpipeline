# Overrides common/import.sh:DoSpecialImport, which executes protocol-specific
# import actions on a particular set of objects defined in paths.txt.
#
# This generic import script is a starting point to handle both basic pants and
# non-pants protocols.
#
# Usage
# -----
# This file is included and used from import.sh at the root-level directory.
#
# Depends:
# - paths.txt - this file defines protocol-specific pathnames to import
function DoSpecialImport
{
    local PROTOCOL=$1
    source protocols/${PROTOCOL}/paths.txt
    echo Do special imports...
if $DRY_RUN; then
    if [[ -e ${IMPORT_METRIC} ]]; then
        echo cp ${IMPORT_METRIC} subjects/${SUBJECT}/protocols/${PROTOCOL}/
    else
        Message "No metric to import for subject ${SUBJECT}."
    fi

    # Protocol-specific setup
    echo cp ${IMPORT_CONES} subjects/${SUBJECT}/protocols/${PROTOCOL}/
    echo cp ${IMPORT_SEAM_DIR}/* subjects/${SUBJECT}/protocols/${PROTOCOL}/seam/
    if $IS_PANTS_PROTOCOL; then
        for CURVE in ${IMPORT_CURVES[@]}; do
            echo mv subjects/${SUBJECT}/protocols/${PROTOCOL}/seam/`basename ${CURVE}` subjects/${SUBJECT}/protocols/${PROTOCOL}/cuts/
        done
    fi
else # DRY_RUN
    if [[ -e ${IMPORT_METRIC} ]]; then
        cp ${IMPORT_METRIC} subjects/${SUBJECT}/protocols/${PROTOCOL}/
    else
        Message "No metric to import for subject ${SUBJECT}."
    fi

    # Protocol-specific setup
    source protocols/${PROTOCOL}/paths.txt
    cp ${IMPORT_CONES} subjects/${SUBJECT}/protocols/${PROTOCOL}/
    cp ${IMPORT_SEAM_DIR}/* subjects/${SUBJECT}/protocols/${PROTOCOL}/seam/
    if $IS_PANTS_PROTOCOL; then
        for CURVE in ${IMPORT_CURVES[@]}; do
            mv subjects/${SUBJECT}/protocols/${PROTOCOL}/seam/`basename ${CURVE}` subjects/${SUBJECT}/protocols/${PROTOCOL}/cuts/
        done
    fi
fi # DRY_RUN
}
