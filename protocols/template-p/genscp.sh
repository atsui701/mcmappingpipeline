# The map goes between intermediate pants meshes rather than the originals.
function CallGenscp
{
    SUBJECT1=$1
    SUBJECT2=$2
    PROTOCOL=$3
    REL_SRC_MESH=$(RelPath   `dirname ${SRC_PANTS_MESH}`  `dirname ${INITIAL_SCP}`)/`basename ${SRC_PANTS_MESH}`
    REL_TGT_MESH=$(RelPath   `dirname ${TGT_PANTS_MESH}`  `dirname ${INITIAL_SCP}`)/`basename ${TGT_PANTS_MESH}`
    REL_TGT_METRIC=$(RelPath   `dirname ${TGT_METRIC}`  `dirname ${INITIAL_SCP}`)/`basename ${TGT_METRIC}`

    SRC_CURVE_LANDMARKS=( ${SRC_CUT_DIR}/*.asc )
    TGT_CURVE_LANDMARKS=( ${TGT_CUT_DIR}/*.asc )
    LANDMARK_PAIRS=""
    for (( i=0; i<${#SRC_CURVE_LANDMARKS[@]}; ++i ));
    do
        LA=${SRC_CURVE_LANDMARKS[$i]}
        LA=$(RelPath `dirname ${LA}` `dirname ${INITIAL_SCP}`)/`basename ${LA}`
        LB=${TGT_CURVE_LANDMARKS[$i]}
        LB=$(RelPath `dirname ${LB}` `dirname ${INITIAL_SCP}`)/`basename ${LB}`
        LANDMARK_PAIRS="${LANDMARK_PAIRS} ${LA} ${LB}"
    done

    if [[ ! -n ${ORBIFOLD_CONES} ]]; then
        CURVE_LANDMARK_FLAGS="--landmarkMode pants ${LANDMARK_PAIRS}"
    else
        REL_SRC_CONES=$(RelPath  `dirname ${SRC_CONES}` `dirname ${INITIAL_SCP}`)/`basename ${ORBIFOLD_CONES}`
        CURVE_LANDMARK_FLAGS="--sourceCones ${REL_SRC_CONES} --landmarkMode cuffs ${LANDMARK_PAIRS}"
    fi

    if $DRY_RUN; then
    echo genscp -s ${REL_SRC_MESH} -t ${REL_TGT_MESH} \
        -u ${REL_TGT_METRIC} \
        -m `basename ${INITIAL_MAPPING}` \
        ${CURVE_LANDMARK_FLAGS} \> ${INITIAL_SCP}
    else
    genscp -s ${REL_SRC_MESH} -t ${REL_TGT_MESH} \
        -u ${REL_TGT_METRIC} \
        -m `basename ${INITIAL_MAPPING}` \
        ${CURVE_LANDMARK_FLAGS} > ${INITIAL_SCP}
    fi
}

