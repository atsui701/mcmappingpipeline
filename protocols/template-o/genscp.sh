function CallGenscp
{
    SUBJECT1=$1
    SUBJECT2=$2
    PROTOCOL=$3
    REL_SRC_MESH=$(RelPath   `dirname ${SRC_MESH}`  `dirname ${INITIAL_SCP}`)/`basename ${SRC_MESH}`
    REL_TGT_MESH=$(RelPath   `dirname ${TGT_MESH}`  `dirname ${INITIAL_SCP}`)/`basename ${TGT_MESH}`
    REL_SRC_CONES=$(RelPath  `dirname ${SRC_FIXED_POINTS}` `dirname ${INITIAL_SCP}`)/`basename ${SRC_FIXED_POINTS}`
    REL_TGT_METRIC=$(RelPath   `dirname ${TGT_METRIC}`  `dirname ${INITIAL_SCP}`)/`basename ${TGT_METRIC}`
    if $DRY_RUN; then
    echo genscp -s ${REL_SRC_MESH} -t ${REL_TGT_MESH} \
        --sourceCones ${REL_SRC_CONES} \
        -u ${REL_TGT_METRIC} \
        -m `basename ${INITIAL_MAPPING}` \> ${INITIAL_SCP}
    else
    genscp -s ${REL_SRC_MESH} -t ${REL_TGT_MESH} \
        --sourceCones ${REL_SRC_CONES} \
        -u ${REL_TGT_METRIC} \
        -m `basename ${INITIAL_MAPPING}` > ${INITIAL_SCP}
    fi
}
