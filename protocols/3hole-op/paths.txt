export IS_PANTS_PROTOCOL=true
# Individual pathnames
export MESH=subjects/${SUBJECT}/mesh.off
export PANTS_MESH=subjects/${SUBJECT}/protocols/${PROTOCOL}/mesh.off
export METRIC=subjects/${SUBJECT}/protocols/${PROTOCOL}/metric.u

# Pairwise pathnames
export SRC_MESH=subjects/${SUBJECT1}/mesh.off
export TGT_MESH=subjects/${SUBJECT2}/mesh.off
export SRC_METRIC=subjects/${SUBJECT1}/protocols/${PROTOCOL}/metric.u
export TGT_METRIC=subjects/${SUBJECT2}/protocols/${PROTOCOL}/metric.u

export SRC_CONES=subjects/${SUBJECT1}/protocols/${PROTOCOL}/cones.asc
export TGT_CONES=subjects/${SUBJECT2}/protocols/${PROTOCOL}/cones.asc
export SRC_SEAM_DIR=subjects/${SUBJECT1}/protocols/${PROTOCOL}/seam
export TGT_SEAM_DIR=subjects/${SUBJECT2}/protocols/${PROTOCOL}/seam

export INITIAL_MAPPING=subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/initial.map
export INITIAL_SCP=${INITIAL_MAPPING%%.map}.scp
export RELAXED_MAPPING=subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/final.map

# Pants protocol only
export CUT_DIR=subjects/${SUBJECT}/protocols/${PROTOCOL}/cuts

export SRC_PANTS_MESH=subjects/${SUBJECT1}/protocols/${PROTOCOL}/mesh.off
export TGT_PANTS_MESH=subjects/${SUBJECT2}/protocols/${PROTOCOL}/mesh.off
export SRC_MPM=subjects/${SUBJECT1}/protocols/${PROTOCOL}/corr.mpm
export TGT_MPM=subjects/${SUBJECT2}/protocols/${PROTOCOL}/corr.mpm

export FINAL_MAPPING=subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/final_original.map

export SRC_ON_TGT_MESH=subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/mesh.off
