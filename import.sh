#!/bin/bash
# Import a subject mesh + landmarks.
#
# Parameters:
#   SUBJECT
#   PROTOCOL
#
# Output:
# subjects/${SUBJECT}/landmarks/{points,curves}
# subjects/${SUBJECT}/protocols
# subjects/${SUBJECT}/protocols/${PROTOCOL}/maps
#
# Additional folders specified in protocols/${PROTOCOL}/scaffold.txt will also
# be created.
#
# Adding a custom protocol
# -----
# Best way is to copy a template folder and 
function usage
{
    echo "Usage: $0 [-hn] subject [protocol]"
    exit 1
}

function Message
{
    echo $1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 1 ]]; then
    usage
fi

SUBJECT=$1
PROTOCOL=$2

# Fully specify the structure of the resources to import
# Non-existing resources will be skipped
#
# from common/import import (DoStandardImport,
#    DoScaffold,
#    DoSpecialImport,
# )
source common/import.sh

DoStandardImport

if [[ $# < 2 ]]; then
    exit 0
fi

if [[ ! -e protocols/${PROTOCOL} ]]; then
    echo Error: protocol ${PROTOCOL} does not exist
    exit 1
fi
DoScaffold ${PROTOCOL}
source protocols/${PROTOCOL}/import.sh
DoSpecialImport ${PROTOCOL}
