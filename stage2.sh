#!/bin/bash
# Execute 2 of 2 of a pants-protocol-based mapping pipeline.
#
# Parameters:
#   SUBJECT1
#   SUBJECT2
#   PROTOCOL
#
# Primary output:
# subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/final.map
#
function usage
{
    echo "Usage: $0 [-hn] subject1 subject2 protocol"
    exit 1
}

function Message
{
    echo $1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

source common/cli.sh
source common/import.sh
source protocols/${PROTOCOL}/import.sh
source protocols/${PROTOCOL}/paths.txt

if [[ ! -e protocols/${PROTOCOL} ]]; then
    echo Error: protocol ${PROTOCOL} does not exist
    exit 1
fi

if [[ -e protocols/${PROTOCOL}/stage2.sh ]]; then
    source protocols/${PROTOCOL}/stage2.sh # defines custom do_stage2
    do_stage2 ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
    exit $?
fi

if $DRY_RUN; then
    echo ./initialmap.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
    echo ./relaxmap.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
else
    ./initialmap.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
    ./relaxmap.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
fi
