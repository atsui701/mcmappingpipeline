#!/bin/bash
# Set up a directory structure for one subject/example.
#
# Protocols are different ways to map surfaces.
#
# Parameters:
#   SUBJECT
#   PROTOCOL
#
# Output:
# subjects/${SUBJECT}/landmarks/{points,curves}
# subjects/${SUBJECT}/protocols
# subjects/${SUBJECT}/protocols/${PROTOCOL}/maps
#
# Additional folders specified in protocols/${PROTOCOL}/scaffold.txt will also
# be created.

#####
# Helper functions
#####

# Populate subfolders common for all subjects
#
# \param subject
function usage
{
    echo "Usage: $0 [-hn] subject [protocol]"
    exit 1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 1 ]]; then
    usage
fi

SUBJECT=$1
PROTOCOL=$2

function ScaffoldCommon
{
    SUBJECT=$1
    if $DRY_RUN; then
        echo mkdir -p subjects/${SUBJECT}/landmarks/{points,curves}
    else
        mkdir -p subjects/${SUBJECT}/landmarks/{points,curves}
    fi
}

# Populate protocol-specific subfolders
#
# \param subject name
# \param protocol name
function ScaffoldProtocol
{
    SUBJECT=$1
    PROTOCOL=$2
    SCAFFOLD_FILE=protocols/${PROTOCOL}/scaffold.txt
    if $DRY_RUN; then
        if [[ ! -e ${SCAFFOLD_FILE} ]]; then
            echo Warning: Scaffold file not found, so just make a default scaffold
            echo mkdir -p subjects/${SUBJECT}/protocols/${PROTOCOL}/maps
            return 0
        fi

        # Evaluate each line in scaffold.txt and create the directory
        for i in `cat ${SCAFFOLD_FILE}`; do
            echo mkdir -p $(eval echo ${i})
        done
    else
        if [[ ! -e ${SCAFFOLD_FILE} ]]; then
            echo Warning: Scaffold file not found, so just make a default scaffold
            echo mkdir -p subjects/${SUBJECT}/protocols/${PROTOCOL}/maps
            mkdir -p subjects/${SUBJECT}/protocols/${PROTOCOL}/maps
            return 0
        fi

        # Evaluate each line in scaffold.txt and create the directory
        for i in `cat ${SCAFFOLD_FILE}`; do
            echo mkdir -p $(eval echo ${i})
            mkdir -p $(eval echo ${i})
        done
    fi
    return 0
}

#####
# Main script body
#####

if [[ $# < 1 ]]; then
    echo "Usage: $0 subject [protocol]"
    exit 0
fi

SUBJECT=$1
PROTOCOL=$2
ScaffoldCommon ${SUBJECT}
if [[ $# > 1 ]]; then
    ScaffoldProtocol ${SUBJECT} ${PROTOCOL}
fi

# o - orbifold
# op - orbifold pants
# p - pants
# c - conformal map
#ScaffoldOrbifold $1 3hole-o
#ScaffoldOrbifoldPants $1 3hole-op
#ScaffoldOrbifoldPants $1 3hole-p
#ScaffoldOrbifoldPants $1 3hole-c


