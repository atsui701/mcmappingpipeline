#!/bin/bash 
# (Pants protocols) Cut meshes along cuts.
#
# Input:
# ${MESH}
# ${CUT_DIR}
#
# Output:
# ${PANTS_MESH}
# ${MPM}

function usage
{
    echo "Usage: $0 [-nc] subject protocol"
    exit 1
}

DRY_RUN=false
CHECK=false
while getopts ":nc" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    c)
        CHECK=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 2 ]]; then
    usage
fi

SUBJECT=$1
PROTOCOL=$2

source common/common.sh
# Load protocol-specific filenames
source protocols/${PROTOCOL}/paths.txt

if ! ${IS_PANTS_PROTOCOL}; then
    echo "Error: ${PROTOCOL} is not a pants protocol."
    exit 1
fi

OUTPUT_DIR=subjects/${SUBJECT}/protocols/${PROTOCOL}
if [[ ! -e ${CUT_DIR} ]]; then
    echo "${CUT_DIR} does not exist"
    exit 1
fi

if $CHECK; then
    if [[ ! -e ${OUTPUT_DIR}/mesh.off ]]; then
        echo ${SUBJECT}
        exit 1
    fi
    exit 0
fi

if $DRY_RUN; then
    echo cutPants -d -m subjects/${SUBJECT}/mesh.off -c ${CUT_DIR} -o ${OUTPUT_DIR}
else
    cutPants -d -m subjects/${SUBJECT}/mesh.off -c ${CUT_DIR} -o ${OUTPUT_DIR}
fi

#Usage: cutPants -m mesh.off -c /path/to/cuts/dir -o /path/to/output/dir 
#Transform a mesh into pants by cutting along cuts.                      
#                                                                        
#Usage: cutPants -m mesh.off -c /path/to/cuts/dir -o /path/to/output/dir 
#                                                                        
#The cuts directory should contain .asc files to cut along.              
#                                                                        
#The output directory will be populated with two files:                  
#                                                                        
#* mesh.off - the pants mesh                                             
#* corr.mpm - the MeshPantsMap from original to pants mesh               
#                                                                        
#  -h [ --help ]         print this message
#  -m [ --mesh ] arg     mesh file
#  -c [ --cuts ] arg     directory containing cut files
#  -o [ --output ] arg   output directory
#  -d [ --debug ]        enable debug messages
