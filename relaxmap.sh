#!/bin/bash

###
# Script initialization
###
function usage
{
    echo "Usage: $0 [-n] source target protocol"
    exit 1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

source common/common.sh
# Load protocol-specific filenames
source protocols/${PROTOCOL}/paths.txt

if $DRY_RUN; then
    echo relaxMap -v --iterations 50 -i ${INITIAL_SCP} -o ${RELAXED_MAPPING} --disableLimitStepSize
else
    timelimit -s 2 -t 3600 -T 3900 relaxMap -v --iterations 50 -i ${INITIAL_SCP} -o ${RELAXED_MAPPING} --disableLimitStepSize
fi
