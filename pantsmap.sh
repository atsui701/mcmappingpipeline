#!/bin/bash
# Construct a map via pants.
#
# Parameters:
#   SUBJECT1 - source
#   SUBJECT2 - target
#   PROTOCOL - protocol
#
# Input (X = 1,2):
# ${SUBJECTX}/mesh.off
# ${SUBJECTX}/protocols/${PROTOCOL}/mesh.off
# ${SUBJECTX}/protocols/${PROTOCOL}/corr.mpm
# ${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/final.map
#
# Output:
# ${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/final_original.map

####################
### Helper functions
####################
function usage
{
    echo "Usage: $0 [-n] source target protocol"
    exit 1
}

####################
### Main script body
####################
DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

# Load protocol-specific filenames
#while read; do
#    $(eval echo "$REPLY")
#done < protocols/${PROTOCOL}/paths.txt
source protocols/${PROTOCOL}/paths.txt

if ! $IS_PANTS_PROTOCOL; then
    echo "Error: ${PROTOCOL} is not a pants protocol."
    exit 1
fi
#$(eval echo `cat protocols/${PROTOCOL}/paths.txt`)

echo mapViaPants -d -s ${SRC_MESH} -S ${TGT_MESH} \
    -p ${SRC_PANTS_MESH} -P ${TGT_PANTS_MESH} \
    -m ${SRC_MPM} -M ${TGT_MPM} \
    -f ${RELAXED_MAPPING} \
    -o ${FINAL_MAPPING}

if $DRY_RUN; then
    exit 0
fi

mapViaPants -d -s ${SRC_MESH} -S ${TGT_MESH} \
    -p ${SRC_PANTS_MESH} -P ${TGT_PANTS_MESH} \
    -m ${SRC_MPM} -M ${TGT_MPM} \
    -f ${RELAXED_MAPPING} \
    -o ${FINAL_MAPPING}


#Error: the option '--mapping' is required but missing
#Create a map between two meshes using a map between their pants. 
#                                                                 
#Usage: mapViaPants -s s1.off -S s2.off -p p1.off -P p2.off \    
#    -m s1_p1.mpm -M s2_p2.mpm -f p1_p2.map -o s1_s2.map    \    
#                                                                 
#  -h [ --help ]                print this message
#  -s [ --sourceMesh ] arg      source mesh file
#  -S [ --targetMesh ] arg      target mesh file
#  -p [ --sourcePantsMesh ] arg source pants mesh file
#  -P [ --targetPantsMesh ] arg target pants mesh file
#  -m [ --sourceMPM ] arg       source mpm file
#  -M [ --targetMPM ] arg       target mpm file
#  -f [ --mapping ] arg         mapping file (between pants)
#  -o [ --output ] arg          output mapping file
#  -d [ --debug ]               enable debug messages

