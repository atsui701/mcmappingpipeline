#!/bin/bash
# Clone the results of stage1 of one protocol into another.
#
# Parameters:
#   SUBJECT
#   PROTOCOL1
#   PROTOCOL2
#
# Input:
# subjects/${SUBJECT}/protocols/{PROTOCOL1}
#
# Output:
# subjects/${SUBJECT}/protocols/{PROTOCOL2}
#
function usage
{
    echo "Usage: $0 [-hnf] subject protocol1 protocol2"
    exit 1
}

DRY_RUN=false
FORCE=false
while getopts ":nf" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    f)
        FORCE=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT=$1
PROTOCOL1=$2
PROTOCOL2=$3

# Fully specify the structure of the resources to import
# Non-existing resources will be skipped
#
# from common/import import (DoStandardImport,
#    DoScaffold,
#    DoSpecialImport,
# )
source common/import.sh

if [[ ! -e protocols/${PROTOCOL1} ]]; then
    echo Error: protocol ${PROTOCOL1} does not exist
    exit 1
fi

if [[ ! -e subjects/${SUBJECT}/protocols/${PROTOCOL1} ]]; then
    echo Error: ${SUBJECT} does not have protocol ${PROTOCOL1}
    exit 1
fi

if [[ -e subjects/${SUBJECT}/protocols/${PROTOCOL2} ]]; then
    if $FORCE; then
        if $DRY_RUN; then
            echo rm -r subjects/${SUBJECT}/protocols/${PROTOCOL2}
        else
            rm -r subjects/${SUBJECT}/protocols/${PROTOCOL2}
        fi
    else
        echo Error: ${SUBJECT} already has protocol ${PROTOCOL2}
        echo "Use the -f flag to replace it"
        exit 1
    fi
fi

if $DRY_RUN; then
    echo mkdir -p subjects/${SUBJECT}/protocols/${PROTOCOL2}/
    echo cp -r subjects/${SUBJECT}/protocols/${PROTOCOL1}/{cuts,seam,corr.mpm,metric.u,cones.asc,mesh.off} subjects/${SUBJECT}/protocols/${PROTOCOL2}/
else
    mkdir -p subjects/${SUBJECT}/protocols/${PROTOCOL2}/
    cp -r subjects/${SUBJECT}/protocols/${PROTOCOL1}/{cuts,seam,corr.mpm,metric.u,cones.asc,mesh.off} subjects/${SUBJECT}/protocols/${PROTOCOL2}/
fi
