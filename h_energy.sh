#!/bin/bash
# (Hyperbolic protocols) Compute harmonic energy of the final map.
#
# Input:
# ${FINAL_SCP}}
#
# Output:
# ${MAP_DIR}/stats/final.energy

function usage
{
    echo "Usage: $0 [-nc] subject1 subject2 protocol"
    exit 1
}

DRY_RUN=false
CHECK=false
while getopts ":nc" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    c)
        CHECK=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

source common/common.sh
# Load protocol-specific filenames
source protocols/${PROTOCOL}/paths.txt

if ! ${IS_PANTS_PROTOCOL}; then
    echo "Error: ${PROTOCOL} is not a pants protocol."
    exit 1
fi

FINAL_MAP=subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/final.scp
OUTPUT_DIR=subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/stats
OUTPUT_FN=final.energy

if [[ ! -e ${FINAL_MAP} ]]; then
    echo "Error: no final map from ${SUBJECT1} to ${SUBJECT2} to work with"
    exit 1
fi

if $CHECK; then
    if [[ ! -e ${OUTPUT_DIR}/${OUTPUT_FN} ]]; then
        echo ${SUBJECT}
        exit 1
    fi
    exit 0
fi

if $DRY_RUN; then
    if [[ ! -e ${OUTPUT_DIR} ]]; then
        echo mkdir -p ${OUTPUT_DIR}
    fi
    echo relaxMap -i ${FINAL_MAP} --computeEnergyOnly -o ${OUTPUT_DIR}/${OUTPUT_FN}
else
    if [[ ! -e ${OUTPUT_DIR} ]]; then
        mkdir -p ${OUTPUT_DIR}
    fi
    relaxMap -i ${FINAL_MAP} --computeEnergyOnly -o ${OUTPUT_DIR}/${OUTPUT_FN}
fi
