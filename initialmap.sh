#!/bin/bash
# Construct an initial map. Also set up scp files.
#
# Parameters:
#   SUBJECT1 - source
#   SUBJECT2 - target
#   PROTOCOL - protocol
#
# Input (X = 1,2):
# ${SRC_MESH}
# ${TGT_MESH}
# ${SRC_SEAM_DIR}/*.asc
# ${TGT_SEAM_DIR}/*.asc
# ${SRC_CONES}
# ${TGT_CONES}
#
# Output:
# ${INITIAL_MAPPING}
# ${INITIAL_SCP}

###
# Script initialization
###
function usage
{
    echo "Usage: $0 [-n] source target protocol"
    exit 1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

source common/common.sh
# Load protocol-specific filenames
source protocols/${PROTOCOL}/paths.txt
source protocols/${PROTOCOL}/genscp.sh # for CallGenscp

CORNER_ORDERING=`cat protocols/${PROTOCOL}/firstCornerOrdering.txt`

####################
### Main script body
####################

if [[ ! -e `dirname ${INITIAL_MAPPING}` ]]; then
    mkdir -p `dirname ${INITIAL_MAPPING}`
fi

if $IS_PANTS_PROTOCOL; then
    if $DRY_RUN; then
    echo generateInitialMap -v -m ${SRC_PANTS_MESH} -M ${TGT_PANTS_MESH} \
        -s ${SRC_SEAM_DIR} -S ${TGT_SEAM_DIR} \
        -c ${SRC_CONES} -C ${TGT_CONES} \
        -f ${CORNER_ORDERING} \
        -o ${INITIAL_MAPPING}
    else
    generateInitialMap -v -m ${SRC_PANTS_MESH} -M ${TGT_PANTS_MESH} \
        -s ${SRC_SEAM_DIR} -S ${TGT_SEAM_DIR} \
        -c ${SRC_CONES} -C ${TGT_CONES} \
        -f ${CORNER_ORDERING} \
        -o ${INITIAL_MAPPING}
    fi
else
    if $DRY_RUN; then
    echo generateInitialMap -v -m ${SRC_MESH} -M ${TGT_MESH} \
        -s ${SRC_SEAM_DIR} -S ${TGT_SEAM_DIR} \
        -c ${SRC_CONES} -C ${TGT_CONES} \
        -f ${CORNER_ORDERING} \
        -o ${INITIAL_MAPPING}
    else
    generateInitialMap -v -m ${SRC_MESH} -M ${TGT_MESH} \
        -s ${SRC_SEAM_DIR} -S ${TGT_SEAM_DIR} \
        -c ${SRC_CONES} -C ${TGT_CONES} \
        -f ${CORNER_ORDERING} \
        -o ${INITIAL_MAPPING}
    fi
fi

CallGenscp ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}

#genscp -s SRC_MESH -t TGT_MESH MAPPING [SRC_LANDMARKS1 TGT_LANDMARKS1 ...]
#
#Produces SCP (Surface Correspondence Project) files, which are batches 
#of filenames pointing to different pieces of data that describe a      
#correspondence.                                                        
#                                                                       
#Example:                                                               
#    genscp -s 1-123/011298/L/mesh.off -t 1-119/022297/L/mesh.off     \
#        -u 1-119/022297/L/orbifolds/8sulci/factors.u                 \
#        -m 1-123/011298/L/maps/1-119.map                               
#                                                                       
#Corresponding landmarks can be specified in pairs.                     
#
#Example:                                                               
#    genscp -s 1-123/011298/L/mesh.off -t 1-119/022297/L/mesh.off     \
#        -m 1-123/011298/L/maps/1-119.map                             \
#        1-119/011298/L/landmarks/1.asc                               \
#        1-123/022297/L/landmarks/1.asc                               \
#        1-119/011298/L/landmarks/2.asc                               \
#        1-123/022297/L/landmarks/2.asc                                 
#
#  -h [ --help ]             print this message
#  -v [ --version ]          version
#  -s [ --sourceMesh ] arg   source mesh
#  -t [ --targetMesh ] arg   target mesh
#  -m [ --mapping ] arg      mapping
#  -u [ --targetMetric ] arg target metric
#  --landmarks arg           landmark pairs
#  --sourceCones arg         source cones
#  --targetCones arg         target cones

#Generate initial maps from a source to target surface mesh.            
#                                                                       
#The strategy is to cut each mesh along a 'seam' into a 'patch' and pin 
#it out at k 'cones' spaced at regular intervals. This identifies each  
#mesh with a regular polygon, and the overlay of one on the other       
#determines the initial map.                                            
#                                                                       
#The patch can be computed (specify the seams, cones, and the cone      
#ordering), or a precomputed one can be used (specify a kpp patch       
#file).                                                                 
#                                                                       
#Example:                                                               
#                                                                       
## computing initial map by specifying the cutting                      
#generateInitialMap -m mesh1.off -M mesh2.off                         \
#    -s seam1/ -S seam2/                                              \
#    -c cones1.asc -C cones2.asc                                      \
#    -f 0,1,2                                                         \
#    -o mesh1_to_mesh2.map                                              
#                                                                       
## using precomputed patches                                            
#generateInitialMap -m mesh1.off -M mesh2.off                         \
#    -p mesh1.kpp -P mesh2.kpp                                        \
#    -o mesh1_to_mesh2.map                                              
#                                                                       
#  -h [ --help ]                  print this message
#  -v [ --verbose ]               verbose
#  --version                      version
#  -m [ --source ] arg            source mesh file
#  -M [ --target ] arg            target mesh file
#  -s [ --sourceSeam ] arg        source seam dir
#  -S [ --targetSeam ] arg        target seam dir
#  -c [ --sourceCones ] arg       source cones file
#  -C [ --targetCones ] arg       target cones file
#  -f [ --firstConeOrdering ] arg comma separated list of cones specified as 
#                                 [beforeFirst, first, afterFirst]. e.g. '0,1,0'
#                                 means the first cone in the cones file will 
#                                 come before and after while the second cone 
#                                 will be used as the first cone in the initial 
#                                 mapping.
#  -p [ --sourcePatch ] arg       source patch
#  -P [ --targetPatch ] arg       target patch
#  -d [ --saveSourcePatch ] arg   save copy of source patch here
#  -D [ --saveTargetPatch ] arg   save copy of target patch here
#  -o [ --output ] arg            output mapping file

