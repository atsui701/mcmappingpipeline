#!/bin/bash
# Execute 3 of 2 of a pants-protocol-based mapping pipeline.
#
# Parameters:
#   SUBJECT1
#   SUBJECT2
#   PROTOCOL
#
# Primary output:
# subjects/${SUBJECT1}/protocols/${PROTOCOL}/maps/${SUBJECT2}/mesh.off
#
function usage
{
    echo "Usage: $0 [-hn] subject1 subject2 protocol"
    exit 1
}

function Message
{
    echo $1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

source common/import.sh
source protocols/${PROTOCOL}/import.sh
source protocols/${PROTOCOL}/paths.txt

if [[ ! -e protocols/${PROTOCOL} ]]; then
    echo Error: protocol ${PROTOCOL} does not exist
    exit 1
fi

if [[ ! -e ${RELAXED_MAPPING} ]]; then
    echo Error: stage2 did not produce a relaxed mapping
    exit 1
fi
if $DRY_RUN; then
    if $IS_PANTS_PROTOCOL; then
        echo ./pantsmap.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
    fi
    echo ./map_mesh.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
else
    if $IS_PANTS_PROTOCOL; then
        ./pantsmap.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
    fi
    ./map_mesh.sh ${SUBJECT1} ${SUBJECT2} ${PROTOCOL}
fi
