#!/bin/bash
# Map a surface onto another.
#
# Parameters:
#   SUBJECT1 - source
#   SUBJECT2 - target
#   PROTOCOL - protocol
#
# Input:
# ${SRC_MESH}
# ${TGT_MESH}
# ${FINAL_MAPPING}
#
# Output:
# ${SRC_ON_TGT_MESH}

####################
### Helper functions
####################
function usage
{
    echo "Usage: $0 [-nm] source target protocol"
    exit 1
}

####################
### Main script body
####################
DRY_RUN=false
while getopts ":nm:" opt; do
    case "${opt}" in
    n)
        DRY_RUN=true
        ;;
    m)
        USER_MAPPING=$OPTARG
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 3 ]]; then
    usage
fi

SUBJECT1=$1
SUBJECT2=$2
PROTOCOL=$3

# Load protocol-specific filenames
#while read; do
#    $(eval echo "$REPLY")
#done < protocols/${PROTOCOL}/paths.txt
#$(eval echo `cat protocols/${PROTOCOL}/paths.txt`)
source protocols/${PROTOCOL}/paths.txt

if [ -n "$USER_MAPPING" ]; then
    MAPPING=${USER_MAPPING}
else
    MAPPING=${FINAL_MAPPING}
fi

echo transferPoints -v -s ${SRC_MESH} -t ${TGT_MESH} \
    -m ${MAPPING} \
    -o ${SRC_ON_TGT_MESH}

if $DRY_RUN; then
    exit 0
fi
transferPoints -v -s ${SRC_MESH} -t ${TGT_MESH} \
    -m ${MAPPING} \
    -o ${SRC_ON_TGT_MESH}

#Error: the option '--mapping' is required but missing
#Transfer points from mesh A to mesh B via a map from A to B.
#
#Usage: transferPoints -s A.off -t B.off -m A_to_B.map [-p A_pts.asc] -o out
#
#If points are specified, only those points are transferred onto B, and
#a points file is written as output. Otherwise, the whole mesh A is
#mapped onto B, and an .off mesh is written as output.
#
#The input map is assumed to be completely specified. i.e. every vertex
#from mesh A is mapped somewhere on mesh B.
#  -h [ --help ]         print this message
#  -s [ --source ] arg   source mesh file
#  -t [ --target ] arg   target mesh file
#  -m [ --mapping ] arg  mapping file
#  -p [ --points ] arg   input points file
#  -o [ --output ] arg   output points file
#  -v [ --verbose ]      verbose

