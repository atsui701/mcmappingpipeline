#!/bin/bash
# Execute 1 of 2 of a pants-protocol-based mapping pipeline.
#
# Parameters:
#   SUBJECT
#   PROTOCOL
#
# Primary output:
# subjects/${SUBJECT}/protocols/${PROTOCOL}/{mesh.off,corr.mpm,metric.u}
#
function usage
{
    echo "Usage: $0 [-hn] subject protocol"
    exit 1
}

function Message
{
    echo $1
}

DRY_RUN=false
while getopts ":n" opt; do
    case "${opt}" in
    h)
        usage
        ;;
    n)
        DRY_RUN=true
        ;;
    *)
        usage
        ;;
    esac
done
shift $((OPTIND-1))

if [[ $# < 2 ]]; then
    usage
fi

SUBJECT=$1
PROTOCOL=$2

source common/cli.sh
source common/import.sh
source protocols/${PROTOCOL}/import.sh
source protocols/${PROTOCOL}/paths.txt

if [[ ! -e protocols/${PROTOCOL} ]]; then
    echo Error: protocol ${PROTOCOL} does not exist
    exit 1
fi

if [[ -e protocols/${PROTOCOL}/stage1.sh ]]; then
    source protocols/${PROTOCOL}/stage1.sh # defines custom do_stage1
    do_stage1 ${SUBJECT} ${PROTOCOL}
    exit $?
fi

if $DRY_RUN; then
    echo ./import.sh ${SUBJECT} ${PROTOCOL}
    if $IS_PANTS_PROTOCOL; then
        echo ./cut.sh ${SUBJECT} ${PROTOCOL}
    fi
    if [[ ! -e ${METRIC} ]]; then
        echo ./metric.sh ${SUBJECT} ${PROTOCOL}
    fi
else
    ./import.sh ${SUBJECT} ${PROTOCOL}
    if $IS_PANTS_PROTOCOL; then
        ./cut.sh ${SUBJECT} ${PROTOCOL}
    fi
    if [[ ! -e ${METRIC} ]]; then
        ./metric.sh ${SUBJECT} ${PROTOCOL}
    fi
fi
