# Edit this path to point to the external data source
IMPORT_SRC_DIR=from/${SUBJECT}
IMPORT_TGT_DIR=../subjects/${SUBJECT}

IMPORT_SRC_DIR=subjects/${SUBJECT}
IMPORT_TGT_DIR=subjects/${SUBJECT}

# Protocol-independent import objects
IMPORT_MESH=${IMPORT_SRC_DIR}/mesh.off
IMPORT_CONES=${IMPORT_SRC_DIR}/landmarks/points/cones.asc
#IMPORT_CURVES=( $(eval echo ${IMPORT_SRC_DIR}/CMARKS/${SUBJECT}/{leye,reye,mouth}.asc) )
IMPORT_CURVES=(
    ${IMPORT_SRC_DIR}/landmarks/curves/{leye,reye,mouth}.asc
)
IMPORT_METRIC=${IMPORT_SRC_DIR}/metric.u
IMPORT_SEAM_DIR=${IMPORT_SRC_DIR}/landmarks/curves/seam
