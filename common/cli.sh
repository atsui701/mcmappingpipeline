function debug
{
    if [[ "$VERBOSE" == true ]]; then
        echo "$*"
    fi
}

function redirect
{
    local OUT=$1
    shift
    if [[ "$DRY_RUN" == true ]]; then
        echo "$*" \>\> ${OUT}
    else
        run "$@" >> ${OUT}
    fi
}

function run
{
    if [[ "$DRY_RUN" == true ]]; then
        echo "$*"
    else
        debug "$*"
        eval "$@"
    fi
}

function create_empty
{
    local OUT=$1
    shift
    if [[ "$DRY_RUN" == true ]]; then
        echo ":" \> ${OUT}
    else
        : > ${OUT}
    fi
}

