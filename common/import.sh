common_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd ${common_DIR} > /dev/null

# define import vars
source import_files.sh

function InitVars
{
    local X=`pwd`
    NAME=`basename $X`
}

function DoScaffold
{
    local PROTOCOL=$1
    if $DRY_RUN; then
    # Generate directory structure
        ./scaffold.sh -n ${SUBJECT} ${PROTOCOL}
    else
    # Generate directory structure
        ./scaffold.sh ${SUBJECT} ${PROTOCOL}
    fi
}

function DoStandardImport
{
    echo Do standard imports...
    if $DRY_RUN; then
    ./scaffold.sh -n ${SUBJECT}
#    # Copy geometry
#    if [[ -e ${IMPORT_MESH} ]]; then
#        echo cp ${IMPORT_MESH} subjects/${SUBJECT}/mesh.off
#    fi
#
#    # Copy landmarks
#    if [[ -e ${IMPORT_CONES} ]]; then
#        echo cp ${IMPORT_CONES} subjects/${SUBJECT}/landmarks/points/
#    fi
#    for CURVE in ${IMPORT_CURVES[@]}; do
#        echo cp ${CURVE} subjects/${SUBJECT}/landmarks/curves/
#    done
    else # DRY_RUN
    ./scaffold.sh ${SUBJECT}
    # Copy geometry
#    if [[ -e ${IMPORT_MESH} ]]; then
#        cp ${IMPORT_MESH} subjects/${SUBJECT}/mesh.off
#    fi
#
#    # Copy cones as point landmarks
#    if [[ -e ${IMPORT_CONES} ]]; then
#        cp ${IMPORT_CONES} subjects/${SUBJECT}/landmarks/points/
#    fi
#    # Copy curve landmarks
#    for CURVE in ${IMPORT_CURVES[@]}; do
#        cp ${CURVE} subjects/${SUBJECT}/landmarks/curves/
#    done
    fi # DRY_RUN
}

function DoSpecialImport
{
    echo Do protocol-specific imports...
    echo "Nothing to be done."
}

popd > /dev/null
