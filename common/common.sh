# Compute the relative path to $1 from $2. Depends on python 2.6+.
#
# $1 - dir
# $2 - dir
function RelPath
{
    RES=`python -c "import os.path; print os.path.relpath('${1}', '${2}')"` 
    echo $RES
}

function implode
{
    RES=$(for i in "$@"; do echo $i; done | tr '\n' ';')
    echo ${RES%;*}
}

